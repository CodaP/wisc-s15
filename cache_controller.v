module cache_controller(clk, rst_n, mem_rdy, pc_addr, icache_hit, mem_re, mem_we, mem_addr, icache_re, icache_we, icache_addr);

	input clk, rst_n;

	/*Outputs from unified_mem*/
	input mem_rdy;

	/*Outputs from Program counter*/
	input [15:0] pc_addr;

	/*Outputs from Icache*/
	input icache_hit; 

	/*Inputs to unified_mem*/
	output reg mem_re, mem_we;
	output reg [14:0] mem_addr;

	/*Inputs to Icache*/
	output reg icache_we;
	output reg icache_re;
	output reg [13:0] icache_addr;

	reg [1:0] cur_state, next_state;
	
	localparam IDLE = 2'b00; //initial state
	localparam MISS = 2'b01;
	localparam HIT = 2'b10;

	reg wait_cnt;

	always @(posedge mem_rdy, negedge rst_n) begin
	 if(!rst_n)
	  wait_cnt <= 1'b0;
	 else 
	  wait_cnt <= wait_cnt + 1;
	end

	//assign icache_hit_in = 1'b0;

	always @(posedge clk, negedge rst_n) begin
		if(!rst_n) begin
			cur_state <= IDLE;
		end else begin
			cur_state <= next_state;
		end
	end

	always @(cur_state, mem_re, mem_we, icache_we, wait_cnt) begin
		mem_re = 1'b0;
		mem_we = 1'b0;
		icache_re = 1'b1;
		icache_we = 1'b0;
		
		//int_addr = (cur_state == IDLE)? pc_addr : int_addr;

		mem_addr = 14'bx;
		icache_addr = 13'bx;
		
		//next_state = START;

		case(cur_state)
		
			IDLE: begin
	 			if(icache_hit) begin //if icache hits, read instruction from icache
						icache_addr = pc_addr[15:2];
						next_state = HIT;
				end 
				else begin //if icache misses, access mem to load needed instructions to Icache			
						next_state = MISS;							
				end
			end

			HIT: begin
				next_state = IDLE;
			end

			MISS: begin
				if(mem_rdy) begin //mem operation is complete and wait for data
						icache_we = 1'b1;
						icache_re = 1'b0;
						icache_addr = pc_addr[15:2];
						mem_re = 1'b1;
						mem_addr = pc_addr[15:1];
						next_state = (!wait_cnt & rst_n)? IDLE : MISS;
				end else begin
						next_state = MISS;
				end
			end									
		endcase
	end
	
endmodule
			
