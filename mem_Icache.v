/*module includes unified_mem, Icache and cache_controller*/
module mem_Icache(clk, rst_n, icache_rd_en, pc_addr, cache_hazard, instr);

	input clk, rst_n;
	input icache_rd_en;
	input [15:0] pc_addr;

	output [15:0] instr;
	output cache_hazard;

	wire mem_re, mem_we, mem_rdy;
	wire [14:0] mem_addr;
	wire [31:0] mem_wr_data;

	wire icache_re, icache_we, icache_wdirty;
	wire [13:0] icache_addr;
	wire [10:0] icache_tag_out;
	wire icache_hit, icache_dirty;

	wire [63:0] icache_wr_data;
	wire [63:0] icache_rd_data;
	wire [31:0] mem_rd_data;

	assign icache_wdirty = 1'b0; //assuming write operation for instr in mem never happens
	assign mem_wr_data = 32'bx;

	assign cache_hazard = rst_n & !mem_rdy;

	unified_mem UM(clk, rst_n, mem_addr, mem_re, mem_we, mem_wr_data, mem_rd_data, mem_rdy);
	cache I_Cache(clk, rst_n, icache_addr, icache_wr_data, icache_wdirty, icache_we, icache_re, icache_rd_data, icache_tag_out, icache_hit, icache_dirty);	
	cache_controller CC(clk, rst_n, mem_rdy, pc_addr, icache_hit, mem_re, mem_we, mem_addr, icache_re, icache_we, icache_addr);

	/*data write from mem to icache*/
	assign icache_wr_data = (!rst_n)? 64'b0 : ((mem_addr[0])? {mem_rd_data, icache_wr_data[31:0]} : {icache_wr_data[63:32], mem_rd_data});
	
	/*pick instruction word*/
	assign instr = (!rst_n)? 16'b0 : (pc_addr[1]? (pc_addr[0]? icache_rd_data[63:48]:icache_rd_data[47:32]):(pc_addr[0]? icache_rd_data[31:16]:icache_rd_data[15:0]));
endmodule

